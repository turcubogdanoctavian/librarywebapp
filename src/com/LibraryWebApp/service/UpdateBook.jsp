<%@page import="com.LibraryWebApp.service.LibraryService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Update Book</h1>

	<h3>Book information:</h3>

	<form method="POST"
		action="http://localhost:8080/LibraryWebApp/UpdateBook">
		Id: <input type="text" name="bookId" value="${book.getId()}">
		Title: <input type="text" name="bookTitle"
			value="${book.getBookTitle()}"> Author: <input type="text"
			name="bookAuthor" value="${book.getBookAuthor()}"> <input
			type="submit" value="Update Book"><input type="hidden"
			name="bookUpdate">
	</form>
	<button type="button" name="back" onclick="history.back()">Back</button>

</body>
</html>