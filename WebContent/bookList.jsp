<%@page import="com.LibraryWebApp.service.LibraryService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Library WebApp</h1>
	<%!int i = 0;%>
	<table>
		<tr>
			<th><c:out value="Id"></c:out></th>
			<th><c:out value="Title"></c:out></th>
			<th><c:out value="Author"></c:out></th>

			<td><c:forEach items="${LibraryService.getBooksStringList() }"
					var="iterator">
					<tr>
						<td><c:out value="${iterator.getId()}"></c:out></td>
						<td><c:out value="${iterator.getBookTitle()}"></c:out></td>
						<td><c:out value="${iterator.getBookAuthor()}"></c:out></td>
						<td><form method="GET"
								action="http://localhost:8080/LibraryWebApp/UpdateBook">
								<input type="submit" value="Update"> <input
									type="hidden" name="bookId" value="${iterator.getId()}">
							</form></td>
						<td><form method="GET"
								action="http://localhost:8080/LibraryWebApp/DeleteBook">
								<input type="submit" value="Delete"> <input
									type="hidden" name="bookId" value="${iterator.getId()}">
							</form></td>
					</tr>
				</c:forEach></td>
	</table>
	<form method="POST"
		action="http://localhost:8080/LibraryWebApp/AddBook">
		Id: <input type="text" name="bookId"> Title: <input
			type="text" name="bookTitle"> Author: <input type="text"
			name="bookAuthor"> <input type="submit" value="Add Book">
	</form>

</body>
</html>