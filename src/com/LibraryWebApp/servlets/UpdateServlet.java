package com.LibraryWebApp.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryWebApp.model.Book;
import com.LibraryWebApp.service.LibraryService;

public class UpdateServlet extends HttpServlet {
	public void init() throws ServletException {
		System.out.println("MyFirstServlet is getting initialized");
	}

	public void destroy() {
		System.out.println("MyFirstServlet is getting destroyed");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("My First GET Request");

		String bookId = request.getParameter("bookId");
		Book book = LibraryService.getBookById(bookId);

		request.setAttribute("book", book);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/UpdateBook.jsp");
		dispatcher.forward(request, response);

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("My First POST Request");

		String bookId = request.getParameter("bookId");
		String bookTitle = request.getParameter("bookTitle");
		String bookAuthor = request.getParameter("bookAuthor");

		LibraryService.updateBook(bookId, bookTitle, bookAuthor);

		RequestDispatcher dispatcher = request.getRequestDispatcher("bookList.jsp");
		dispatcher.forward(request, response);
	}
}
