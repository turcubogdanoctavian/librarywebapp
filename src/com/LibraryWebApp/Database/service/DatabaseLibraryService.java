package com.LibraryWebApp.Database.service;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.LibraryWebApp.model.Book;

public class DatabaseLibraryService {

	private static SessionFactory sessionFactory;

	public static void main(String[] args) {
		sessionFactory = new Configuration().configure().buildSessionFactory();

		sessionFactory.close();
	}

	public static void listBooks() {

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		List<Book> booksList = session.createQuery("from BookList").list();

		for (Book book : booksList) {
			System.out.println(book.getId() + " " + book.getBookTitle() + " " + book.getBookAuthor());
		}

		transaction.commit();

		session.close();

	}

	public static void addBooks(Book book) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(book);

			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	public static void deleteBook(Book book) {

		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();

			session.delete(book);

			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}

	}

	public static void updateBook(Book book) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();

			session.update(book);

			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}

	}

}
