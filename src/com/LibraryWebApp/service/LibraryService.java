package com.LibraryWebApp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.LibraryWebApp.model.Book;

public class LibraryService {

	public static List<Book> booksList = new ArrayList<>();

	public static void listBooks() {
		if (booksList.isEmpty()) {
			System.out.println("Book list is empty");
		} else {
			for (Book book : booksList) {
				System.out.println(book);
			}
		}
	}

	public static List<Book> getBooksStringList() {
		initBookList();
		return booksList;
	}

	private static void initBookList() {
		if (booksList.isEmpty()) {
			Book book1 = new Book(1, "Title1", "Author1", new Date());
			Book book2 = new Book(2, "Title2", "Author2", new Date());
			Book book3 = new Book(3, "Title3", "Author3", new Date());

			booksList.add(book1);
			booksList.add(book2);
			booksList.add(book3);
		}
	}

	public static Book getBookById(String id) {
		int bookId = Integer.parseInt(id);
		for (Book book : booksList) {
			if (book.getId() == bookId) {
				return book;
			}
		}

		return null;
	}

	public static void addBook(Book newBook) {
		booksList.add(newBook);
	}

	public static void deleteBook(Book deletedBook) {
		System.out.println("Book " + deletedBook + " was deleted");
		booksList.remove(deletedBook);
	}

	public static void deleteBookById(String id) {
		int bookId = Integer.parseInt(id);
		for (Book book : booksList) {
			if (bookId == book.getId()) {
				booksList.remove(book);
				break;
			}
		}
	}

	public static void updateBook(String id, String newBookTitle, String newBookAuthor) {
		Book book = getBookById(id);
		book.setBookTitle(newBookTitle);
		book.setBookAuthor(newBookAuthor);
	}
}
