package com.LibraryWebApp.service;

import com.LibraryWebApp.model.*;
import java.util.Date;

import com.LibraryWebApp.service.LibraryService;

public class LibraryServiceController {
	public static void main(String[] args) {

		Book book1 = new Book(1, "Title1", "Author1", new Date());
		Book book2 = new Book(2, "Title2", "Author2", new Date());
		Book book3 = new Book(3, "Title3", "Author3", new Date());

		LibraryService.booksList.add(book1);
		LibraryService.booksList.add(book2);
		LibraryService.booksList.add(book3);

		LibraryService.listBooks();

		Book book4 = new Book(4, "Title4", "Author4", new Date());
		LibraryService.addBook(book4);

		System.out.println(" ");
		LibraryService.listBooks();

		LibraryService.deleteBook(book4);
		System.out.println(" ");
		LibraryService.listBooks();

		// LibraryService.updateBook(book2, 5);
		System.out.println(" ");
		LibraryService.listBooks();
	}
}
