package com.LibraryWebApp.servlets;

import com.LibraryWebApp.model.*;
import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryWebApp.service.LibraryService;

public class Servlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {
		System.out.println("MyFirstServlet is getting initialized");
	}

	public void destroy() {
		System.out.println("MyFirstServlet is getting destroyed");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("My First GET Request");

		String bookId = request.getParameter("bookId");

		LibraryService.deleteBookById(bookId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("bookList.jsp");
		dispatcher.forward(request, response);

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("My First POST Request");

		int bookId = Integer.parseInt(request.getParameter("bookId"));

		String bookTitle = request.getParameter("bookTitle");
		String bookAuthor = request.getParameter("bookAuthor");

		Book book = new Book(bookId, bookTitle, bookAuthor, new Date());

		LibraryService.addBook(book);

		RequestDispatcher dispatcher = request.getRequestDispatcher("bookList.jsp");
		dispatcher.forward(request, response);

	}
}
